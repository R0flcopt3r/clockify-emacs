;;; clockify.el --- Clockify emacs client -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 R0flcopt3r
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.
;;
;; Author: R0flcopt3r <https://gitlab.com/R0flcopt3r>
;; Maintainer: R0flcopt3r
;; Created: February 20, 2021
;; Modified: February 20, 2021
;; Version: 0.0.1
;; Keywords:
;; Homepage: https://gitlab.com/R0flcopt3r/clockify-emacs
;; Package-Requires: ((emacs 27.1) (cl-lib "0.5") (request "20210214.37"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Control timetracking in Clockify from Emacs
;;
;;; Code:
(require 'request)

(defvar clockify-base-api-endpoint "https://api.clockify.me/api/v1"
  "Base endpoint. Change this if you have selfhost.")
(defvar clockify-api-key nil
  "Api key for clockify user.")
(defvar clockify-billable-default nil
  "Default value for billable.")
(defvar clockify--user-id
  (cdr (assoc 'id (clockify--get-user)))
  "User id associated with the API key.")
(defvar clockify--active-workspace
  (cdr (assoc 'activeWorkspace (clockify--get-user)))
  "Current active workspace from clockify.")
(defvar clockify--default-header
  `(("Content-Type" . "application/json")
    ("X-Api-Key" . ,clockify-api-key)))

(defun clockify--get-user ()
  "Get user data."
  (let (json)
    (request (concat clockify-base-api-endpoint "/user")
      :type "GET"
      :sync t
      :headers `(("content-type". "application/json") ("X-Api-Key" . ,clockify-api-key))
      :success (cl-function (lambda (&key data &allow-other-keys) (setq json data)))
      :parser #'json-read)
    json))

(defun clockify-stop-timer ()
  "Stop the timer for a user with `clockify-api-key'."
  (interactive)
  (request (concat
            clockify-base-api-endpoint
            "/workspaces/" clockify--active-workspace
            "/user/" clockify--user-id
            "/time-entries")
    :type "PATCH"
    :headers `(("Content-Type" . "application/json")
               ("X-Api-Key" . ,clockify-api-key))
    :data (json-encode `(("end" . ,(format-time-string "%FT%TZ" nil t))))
    :parser #'json-read
    :success (message "Timer stopped")
    :error (cl-function
            (lambda (&rest args &key error-thrown &allow-other-keys)
              (message "Unexpected error: %S" error-thrown)))
    :status-code '(( 404 . (lambda (&rest _) (message "Clockify: No active timer found"))))))

(defun clockify--get-time-entries (&optional running)
  "Get all time entries. If RUNNING is set, return the current in-progress timer."
  (let (json)
    (request (concat
              clockify-base-api-endpoint
              "/workspaces/" clockify--active-workspace
              "/user/" clockify--user-id
              "/time-entries")
      :type "GET"
      :sync t
      :headers `(("X-Api-Key" . ,clockify-api-key))
      :success (cl-function (lambda (&key data &allow-other-keys)
                              (message "success")
                              (setq json data)))
      :params (if running '(("in-progress" . "true")) nil)
      :parser #'json-read
      :error (cl-function
              (lambda (&rest args &key error-thrown &allow-other-keys)
                (message "Unexpected error: %S" error-thrown))))
    json))

(defun clockify--get-all-projects ()
  "Return all projects for the active workspace."
  (let (json)
    (request (concat
              clockify-base-api-endpoint
              "/workspaces/"
              clockify--active-workspace
              "/projects")
      :type "GET"
      :sync t
      :headers clockify--default-header
      :parser #'json-read
      :success (cl-function (lambda (&key data &allow-other-keys)
                              (message "success")
                              (setq json data)))
      :error (cl-function
              (lambda (&rest args &key error-thrown &allow-other-keys)
                (message "Unexpected error: %S" error-thrown))))

    json))

(defun clockify--select-description ()
  "Interactivly select a description or write a new one."
  (let ((descriptions
         (loop for entry across (clockify--get-time-entries)
               collect (cdr (assoc 'description entry)))))
    (completing-read "Description: " descriptions)))

(defun clockify--select-project-id ()
  "Interactivly select a project by name and return it's ID."
  (let ((project-alist
         (loop for project across (clockify--get-all-projects)
               collect `( ,(cdr (assoc 'name project)) . ,(cdr (assoc 'id project))))))
    (cdr (assoc
          (completing-read "Project: "
                           (loop for (key . value) in project-alist collect key))
          project-alist))))



(defun clockify-add-time-entry ()
  "Interactivly start a new time entry by selecting projects and description.
from a list. There is no option to set start or end time, this will simply start
a timer from current time. Stop timer with `clockify-stop-timer'"
  (interactive)
  (let ((billable nil)
        (description (clockify--select-description))
        (projectid (clockify--select-project-id)))
    (request
      (concat clockify-base-api-endpoint "/workspaces/" clockify--active-workspace "/time-entries")
      :type "POST"
      :headers clockify--default-header
      :success (message "time added")
      :data (json-encode `(("start" . ,(format-time-string "%FT%TZ" nil t ))
                           ("billable" . ,billable)
                           ("description" . ,description)
                           ("projectId" . ,projectid)))
      :parser #'json-read
      :error (cl-function
              (lambda (&rest args &key error-thrown &allow-other-keys)
                (message "Unexpected error: %S" error-thrown))))))

(provide 'clockify)
;;; clockify.el ends here
